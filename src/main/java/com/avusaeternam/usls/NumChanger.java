package com.avusaeternam.usls;

public class NumChanger {
    public NumChanger(){
        this.number = 0;
    }
    public NumChanger(int number) {
        this.number = number;
    }

    private int number;

    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public int doubleNum(){
        this.number *= 2;
        return number;
    }
    public int add(int other){
        number += other;
        return number;
    }
}
