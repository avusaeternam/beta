package com.avusaeternam.lib.algorithm;

public interface UnionFind {
    public void union(int p, int q);

    public boolean connected(int p, int q);
}
