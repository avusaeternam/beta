package com.avusaeternam.lib.algorithm;

public class QuickFind implements UnionFind {
    private int[] nodes;

    public QuickFind(int n) {
        nodes = new int[n];
        for (int i = 0; i < n; i++) {
            nodes[i] = i;
        }
    }

    @Override
    public void union(int p, int q) {
        int pNode = nodes[p];
        int qNode = nodes[q];
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] == qNode) {
                nodes[i] = pNode;
            }
        }
    }

    @Override
    public boolean connected(int p, int q) {
        return nodes[p] == nodes[q];
    }
}
