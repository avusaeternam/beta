package com.avusaeternam.usls;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumChangerTest {
    NumChanger nc;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        this.nc = new NumChanger();
    }

    @org.junit.jupiter.api.Test
    void getNumber() {

    }

    @org.junit.jupiter.api.Test
    void doubleNum() {
    }

    @org.junit.jupiter.api.Test
    void add() {
    }

    @Test
    public void defaultCtorTest(){
        setUp();
        assertEquals(0, nc.getNumber(), "Default ctor set number = 0");
    }

    @Test
    public void doubleNumTest(){
        nc.setNumber(1);
        nc.doubleNum();
        assertEquals(2, nc.getNumber(), "Doubling doubles num");
    }
}